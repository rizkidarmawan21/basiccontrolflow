<?php
function sayHelloFiveTime() {
    $index = 0;
    while($index < 5) {
        echo "Hello ".($index+1)."\n";
        $index++;
    }
}

sayHelloFiveTime();
echo "\n";

// Function names are NOT case-sensitive.
sayHellofivetime();
?>