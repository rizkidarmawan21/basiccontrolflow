<?php
    $x = 50;
    $y = 100;
    $z = "100";

    // Equal ==
    if ($y == $z) {
        echo "Nilai y dan z sama";
    }else {
        echo "Nilai y dan z beda";
    }

    echo "\n";
    
    // Identical ===
    if ($y === $z) {
        echo "Nilai dan type data y dan z sama";
    }else {
        echo "Nilai dan type data y dan z beda";
    }


?>