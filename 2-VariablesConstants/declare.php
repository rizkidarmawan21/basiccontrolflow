<?php
    define("Phi", 3.14159);
    echo Phi;
    echo "\n";

    // case-sensitive
    // echo phi;
    // echo "\n";

    // redeclare
    define("Phi", 3.14);
    echo "\n";

    $gravity = 9.8;
    echo "Gravity: $gravity";
    $gravity = 10;
    echo "\n";
    echo "Gravity: $gravity";
?>