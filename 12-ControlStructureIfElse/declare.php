<?php

// Nested If
$var1 = 10;
$var2 = 20;
    if ($var1 != $var2) 
    {  
      echo "var1 is not equal to var2\n";   
        if ($var1 > $var2) 
        {   
          echo "var1 is greater than var2\n";  
        }
        else 
        {   
          echo "var2 is greater than var1\n";
        } 
    } 
    else 
    {  
      echo "var1 is equal to var2"; 
    }
    
/* Output:
var1 is not equal to var2
var2 is greater than var1
*/

?>
?>