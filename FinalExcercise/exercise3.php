<?php

?>
<!-- 
Deskripsi
Diberikan sebuah nilai uang sebesar K rupiah, buatlah sebuah 
program yang akan menghasilkan koin-koin bernilai total K 
dengan memakai koin bernilai sebesar-besarnya. Jika koin 
terbesar tidak dapat dipakai (karena jumlah koin akan melebihi K), 
maka ambillah koin yang lebih kecil berikutnya, dan seterusnya.

Koin-koin yang tersedia bernilai 1, 2, 5, 10, 20, 50, 100 rupiah


Output
1. Jika K = 98 maka outputnya:
50 1
20 2
5 1
2 1
1 1

2. Jika K = 79 maka outputnya:
50 1
20 1
5 1
2 2


 -->