<?php

?>
<!-- 
Deskripsi
Terdapat segi empat yang keempat sudutnya siku-siku. 
Segi empat tersebut mempunyai luas NN dan panjang 
sisi-sisinya merupakan bilangan bulat. Carilah panjang 
dua sisi yang saling tegak lurus yang mungkin pada segi 
empat tersebut.


Output
1. Jika N = 77 maka outputnya:
7 11

2. Jika N = 12 maka outputnya:
6 2

3. Jika N = 1 maka outputnya:
1 1
 -->