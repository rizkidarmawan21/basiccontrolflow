<?php

?>
<!-- 
Deskripsi
Pak Oski kedatangan tamu dari luar kota. Tamu yang datang 
ternyata adalah adik Pak Oski. Adik Pak Oski selalu datang 
beserta dua anaknya. Dikarenakan keponakan Pak Oski masih 
kecil, mereka sangat senang bermain. Keponakannya bernama 
Mimik dan Kikil. Agar mereka dapat bermain Pak Oski 
mengeluarkan setumpuk gelas plastik untuk disusun. 
Mimik dan Kikil bergantian mengambil 1 gelas dari tumpukan 
tersebut 1 per 1, dan membangun tumpukan sendiri. Ketika 
semua gelas telah dipakai, terkadang salah satu tumpukan 
gelas lebih tinggi dari yang lainnya. Hal ini mengakibatkan 
salah satu keponakannya sedih dan menangis. Tak ingin hal 
tersebut terjadi, Pak Oski terkadang harus mengambil 1 gelas 
dahulu dan kemudian sisanya diberikan kepada 2 keponakannya. 
Namun karena terlalu sibuk berbicara dengan adiknya, 
Pak Oski bisa salah menentukan perlu atau tidaknya sebuah 
gelas diambil. Bantulah Pak Oski untuk menentukan perlu atau 
tidaknya sebuah gelas tersebut diambil sebelum diberikan ke 
Mimik dan Kikil.


Output
1. Jika N = 77 maka outputnya:
7 11

2. Jika N = 12 maka outputnya:
6 2

3. Jika N = 1 maka outputnya:
1 1
 -->