<?php

?>
<!-- 
Deskripsi
Blank-on adalah band pop yang sedang naik daun di Indonesia. 
Pekan ini, Blank-on akan melakukan tur konser musik besar-besaran 
di sejumlah kota besar di Indonesia. Blank-on divokali oleh 
seorang wanita muda energik bernama Kei.

Kei sedang kebingungan memilih baju. Di lemari baju di belakang 
panggung, ada banyak sekali baju. Kei sangat benci menggunakan 
baju yang terlalu kecil karena akan mengganggu gerakannya 
selama di panggung. Bila tidak ada ukuran baju yang cocok, 
dia lebih memilih menggunakan baju yang ukurannya sedikit 
lebih besar. Tetapi dia tidak mau memakai baju yang terlalu 
besar karena penonton sangat memerhatikan fashion personil 
Blank-on, apalagi Kei seorang vokalis.

Jika tak ada baju yang pas/cocok untuk Kei, beri dia ukuran X. 
Jika ukuran tubuh Kei terlalu kecil untuk ukuran apapun, 
beri ukuran S. Ukuran baju yang tersedia adalah sebagai berikut.

                Ukuran Bahu     Panjang     Ukuran lingkar baju
S (Small)           10             40               90
M (Medium)          14             60               120 
L (large)           18             80               180
X (extra Large)     25             100              220

Diberikan ukuran bahu (B), panjang (P), lingkar baju (L) Kei. 
Pilihkan ukuran baju yang paling tepat untuk Kei, dan jangan 
buat fans Blank-on kecewa!

Output
1. jika B = 10, P = 50, L = 100 => M (Medium)
2. jika B = 18, P = 80, L = 180 => L (Large)

Rule:
1. 1 ≤ B, P, L ≤ 200
 -->