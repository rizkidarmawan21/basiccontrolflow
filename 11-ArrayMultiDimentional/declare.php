<?php
    $footballers = array(
        "defender" => array("Meguire", "Martinez", "Varane"),
        "midfielder" => array("Casemiro", "Ericksen", "McTominay"),
        "forward" => array("Fernandes", "Ronaldo", "Rushford"),
    );

    echo "Best Defender: ";
    echo $footballers['defender'][0];
    echo "\n";
    echo "Best Forward: ";
    echo $footballers['forward'][1];
?>