<?php
// Global Variable
$first_name = 'Federer';
function getName() {
    // Local Variable
    $last_name = "Roger";
    echo $last_name;
    echo "\n";
    // Error
    echo $first_name;
    echo "\n";
    // Call Global Variable
    global $first_name;
    echo $first_name;
    echo "\n";
}
getName();

?>