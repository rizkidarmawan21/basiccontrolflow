<?php
$age = 30;
$first_name = "Cindy";
$snack1 = 10;

echo "Iam $age years old";
echo "\n";

print "My name is $first_name";
print "\n";

echo "snack 1: $snack1";
echo "\n";

$snack1 = 11;
echo "snack 1: $snack1 (change)";

// case-sensitive
$snack2 = 15;
print "snack 2: $Snack2";
print "\n";

// start with a number
$9snack = 7;
print "snack : $9snack";
print "\n";


// Starts with a dollar sign ($) and followed by the name
// Start with a letter or an underscore
// Cannot start with a number
// Only contain alpha-numeric characters and underscores (A-z, 0-9, and _ )
// case-sensitive ($name and $NAME would be two different variables)
?>

